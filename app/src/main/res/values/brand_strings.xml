<?xml version="1.0" encoding="utf-8"?><!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
<!DOCTYPE resources [<!ENTITY brandShortName "CENO"><!ENTITY  brandFullName   "eQualitie CENO"><!ENTITY  vendorShortName "eQualitie">]>
<resources xmlns:tools="http://schemas.android.com/tools">
    <string name="app_name" translatable="false">&brandShortName;</string>

    <!-- Preference for settings related to changing the default browser -->
    <string name="preferences_make_default_browser">Make &brandShortName; your default browser</string>

    <!-- Preference about page -->
    <string name="preferences_about_page">About &brandShortName;</string>

    <!-- Preference category for Mozilla about/legal etc. -->
    <string name="vendor_category" translatable="false">&vendorShortName;</string>

    <!-- About content -->
    <string name="about_content"><![CDATA[
<p>%1$s puts you in control.</p>
<p>Use it as a reference to:
    <br>&bull; Learn how to build your own browser.
    <br>&bull; Contribute to making it easier to build browsers.
    <br>&bull; File bug reports!
</p>
<p>%1$s is produced by eQualitie. Our mission is to .<br/>
    ]]></string>

    <!-- Home page browsing mode reminder. -->
    <string name="ceno_home_mode_description">Use normal tabs to share content with other &brandShortName; users. Use private tabs to avoid sharing, or for pages that require login.</string>
    <string name="ceno_home_mode_link_text">Tap here to learn more.</string>

    <!-- TRANSLATORS: Keep the URL as is unless the section has been translated. -->
    <string name="ceno_mode_manual_link">https://censorship.no/user-manual/en/concepts/public-private.html</string>

    <!-- Extra paragraph in private tab panel. -->
    <!-- <string name="ceno_private_tab_panel_description">&brandShortName; will not share content from private tabs with other users. You can safely visit sites that require login. A path to the Internet is needed, though.</string> -->

    <string name="ceno_private_browsing_body"><![CDATA[
<h1>Private Browsing</h1>
<p>Ceno will not share content from private tabs with other users. You can safely visit sites that require login. A path to the Internet is needed, though.</p>
<p class="about-info"><a id="learnMore">Want to learn more?</p>
     ]]></string>

    <!-- Service notification and actions -->
    <string name="ceno_notification_title">&brandShortName; Browser Service</string>
    <string name="ceno_notification_description">Tap to stop</string>
    <string name="ceno_notification_home_description">Home</string>
    <string name="ceno_notification_clear_description">Erase&#9888;</string>
    <string name="ceno_notification_clear_do_description">Yes</string>
    <string name="ceno_notification_channel_name">&brandShortName; Service</string>

    <!--
    <string name="preferences_mobile_data" translatable="false">Do not show mobile data warning</string>
    <string name="preferences_mobile_data_warning_disabled" translatable="false">Mobile data warning disabled</string>
    <string name="preferences_mobile_data_warning_enabled" translatable="false">Mobile data warning enabled</string>
    -->

    <!-- "On mobile data" dialog -->
    <string name="ceno_on_mobile_data_dialog_title">&brandShortName; Browser notice</string>
    <string name="ceno_on_mobile_data_dialog_description">It is recommended to use &brandShortName; when you have a Wi-Fi connection, as it may use more data than you expect. You may want to stop &brandShortName; when using mobile data.</string>
    <string name="ceno_on_mobile_data_dialog_stop_now">Stop &brandShortName; now</string>
    <string name="ceno_on_mobile_data_dialog_continue">Continue</string>
    <string name="ceno_on_mobile_data_dialog_stop_showing">Do not show again</string>

    <!-- "Purge button" dialog-->
    <string name="ceno_clear_dialog_clear_entire_app">Clear &brandShortName; cache and app data</string>
    <string name="ceno_clear_dialog_cancel">Cancel</string>
    <string name="ceno_clear_dialog_clear_cache_only">Clear &brandShortName; cache only</string>
    <string name="ceno_clear_dialog_title">Clear &brandShortName;</string>
    <string name="ceno_clear_dialog_description">WARNING: Clearing cache and app data will reset the &brandShortName; application as if it was never used.</string>
    <string name="ceno_clear_action_description">Clear</string>

    <!-- Menu options -->
    <!-- <string name="browser_menu_report_issue" translatable="false">Report issue</string> -->
    <string name="browser_menu_https_by_default" translatable="false">HTTPS by default</string>
    <string name="browser_menu_ublock_origin" translatable="false">uBlock Origin</string>
    <string name="browser_menu_ceno_ext">&brandShortName; Settings</string>

    <!-- Default suggested site title and urls -->
    <string name="default_top_site_1_title">&brandShortName; Manual</string>
    <string name="default_top_site_1_url" translatable="false">@string/suggestedsites_ceno_en_url</string>
    <string name="default_top_site_2_title" translatable="false">Wikipedia</string>
    <string name="default_top_site_2_url" translatable="false">@string/suggestedsites_wikipedia_url</string>
    <string name="default_top_site_3_title" translatable="false">AP News</string>
    <string name="default_top_site_3_url" translatable="false">@string/suggestedsites_apnews_url</string>
    <string name="default_top_site_4_title" translatable="false">Reuters</string>
    <string name="default_top_site_4_url" translatable="false">@string/suggestedsites_reuters_url</string>
    <string name="default_top_site_5_title" translatable="false">BBC News</string>
    <string name="default_top_site_5_url" translatable="false">@string/suggestedsites_bbc_en_url</string>
    <string name="horizontal_ellipsis" translatable="false">&#8230;</string>

    <string name="onboarding_fragment">Welcome to &brandShortName;</string>
    <string name="onboarding_button">Get Started</string>
    <string name="onboarding_skip_button">Skip</string>

    <string name="onboarding_info_title">Not your average browser</string>
    <string name="onboarding_info_text">&brandShortName; uses peer-to-peer technology to share web content that can be accessed by other &brandShortName; users</string>
    <string name="onboarding_info_button">Cool!</string>

    <string name="onboarding_battery_title">Permission needed</string>
    <string name="onboarding_battery_text">Shortly the OS will ask you for permission to disable battery optimization.\n\nThis is required to keep sharing websites while the app is in the background</string>
    <string name="onboarding_battery_button">Continue</string>

    <string name="onboarding_warning_title">Permission denied</string>
    <string name="onboarding_warning_text">Battery optimization is still enabled.\n\nGo to \"General Settings > Disable Battery Optimization\" to give permission later</string>
    <string name="onboarding_warning_button">Okay</string>

    <string name="onboarding_thanks_title">Thanks for growing the network!</string>
    <string name="onboarding_thanks_text">As a &brandShortName; user, you\'re helping to grow the network of available bridges enabling access to censored content.</string>
    <string name="onboarding_thanks_button">Let\s go!</string>

    <string name="preferences_toolbar_position">Place at top of screen</string>
    <string name="preferences_toolbar_hide">Scroll to hide</string>

    <string name="preferences_show_onboarding">Show onboarding on next launch</string>
    <string name="preferences_disable_battery_opt">Disable Battery Optimization</string>

    <string name="preferences_customization">Customization</string>

    <string name="preferences_clear_in_toolbar">Show on toolbar</string>
    <string name="preferences_clear_in_menu">Show in menu</string>

    <string name="preferences_clear_behavior">Default Behavior</string>
    <string name="preferences_clear_behavior_summary">Set behavior on tapping clear button</string>
    <string name="preferences_clear_behavior_default">0</string>
    <string-array name="preferences_clear_behavior_values" >
        <item>0</item>
        <item>1</item>
        <item>2</item>
    </string-array>
    <string-array name="preferences_clear_behavior_options" >
        <item>Prompt</item>
        <item>Clear CENO cache only</item>
        <item>Clear app data and CENO cache</item>
    </string-array>

    <string name="preferences_change_app_icon">Change app icon</string>

    <string name="preferences_theme">Set app theme</string>
    <string name="preferences_theme_summary">Dark, Light, Battery Saving</string>
    <string name="preferences_theme_default">2</string>
    <string-array name="preferences_theme_values" >
        <item>2</item>
        <item>1</item>
        <item>3</item>
    </string-array>
    <string-array name="preferences_theme_options" >
        <item>Dark</item>
        <item>Light</item>
        <item>Set by Battery Saver</item>
    </string-array>

    <!-- Snackbar action button when a non fatal crash happen-->
    <string name="crash_report_non_fatal_action">Tap to report to &vendorShortName;</string>

    <!-- COPIED FROM DUCKDUCKGO -->
    <!-- Change Icon Activity -->
    <string name="changeIconCtaAccept">Apply</string>
    <string name="changeIconCtaCancel">Cancel</string>
    <string name="changeIconDialogTitle">Apply new icon?</string>
    <string name="changeIconDialogMessage">The app may close to apply changes. Come on back after you\'ve admired your handsome new icon.</string>
    <!-- END COPIED FROM DUCKDUCKGO -->

    <!-- Description of the preference that enables/disables search suggestions -->
    <string name="preference_show_search_suggestions_summary">&brandShortName; will send what you type in the address bar to your search engine</string>

    <string name="clear_cache_success">Cache cleared successfully</string>
    <string name="clear_cache_fail">Cache clear failed, try again soon</string>

    <string name="addon_placeholder">No Add-ons Available</string>

</resources>

